#pragma once

float SqrSum(float a, float b)
{
    return (a + b) * (a + b);
}